#include "mainwindow.h"
#include <QApplication>

//nasledujuci kod sa kompiluje iba pod windowsom
#ifdef _WIN32
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
extern "C" {
_declspec(dllexport) uint32_t NvOptimusEnablement = 0x00000001;
}
extern "C"
{
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}
#endif

int main(int argc, char *argv[])
{
	//nastavenie nazvu a vlastnosti aplikacie
	QCoreApplication::setOrganizationName("SvF STU");
	QCoreApplication::setOrganizationDomain("svf.stuba.sk");
	QCoreApplication::setApplicationName("PlochoGen");
	//vytvorim aplikaciu, formular a zobrazim ho
	QApplication a(argc, argv);
	PlochoUI w;
	w.show();

	return a.exec();
}
