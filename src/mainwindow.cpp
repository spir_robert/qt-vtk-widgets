#include "mainwindow.h"
#include "ui_mainwindow.h"


PlochoUI::PlochoUI(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::PlochoUI)
{
	QSurfaceFormat surfaceFormat = QVTKOpenGLWidget::defaultFormat();

	surfaceFormat.setSamples(8);

	surfaceFormat.setStencilBufferSize(8);

	QSurfaceFormat::setDefaultFormat(surfaceFormat);
	ui->setupUi(this);
	this->setWindowTitle("PlochoGen");
	//vtkObject::GlobalWarningDisplayOff();

	//incializacia vtk rendera
	renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkGenericOpenGLRenderWindow> window = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();

	ui->qvtkWidget->SetRenderWindow(window);

	ui->qvtkWidget->GetRenderWindow()->AddRenderer(renderer);

	renderer->ResetCamera();
	renderer->SetBackground(0.2, 0.3, 0.4);
	ui->qvtkWidget->update();
}
//destruktor
PlochoUI::~PlochoUI()
{
    delete ui;
}

void PlochoUI::show()
{
	QWidget::show();

}

//resetovanie kamery zobrazenia
void PlochoUI::reset_viewport_clicked()
{
	this->renderer->ResetCamera();
	this->ui->qvtkWidget->update();
}

//ukoncenie
void PlochoUI::exit_clicked()
{
	this->close();
}

void PlochoUI::UpdateWidget()
{
	this->ui->qvtkWidget->GetRenderWindow()->Render();
	this->ui->qvtkWidget->update();
}

void PlochoUI::button1_clicked()
{
	vtkSmartPointer<vtkSphereSource> sphereSource =
		vtkSmartPointer<vtkSphereSource>::New();
	sphereSource->SetCenter(0.0, 0.0, 0.0);
	sphereSource->SetRadius(5.0);

	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(sphereSource->GetOutputPort());

	vtkSmartPointer<vtkActor> actor =
		vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	
	
	renderer->AddActor(actor);

	this->renderer->ResetCamera();
	this->UpdateWidget();
}

