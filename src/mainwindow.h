#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <random>

// Qt
#include <QMainWindow>
#include <QtCore>
#include <QFileDialog>
#include <QWindow>

// Visualization Toolkit (VTK)
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkChartXY.h>
#include <vtkTable.h>
#include <vtkPlot.h>
#include <vtkFloatArray.h>
#include <vtkContextView.h>
#include <vtkContextScene.h>
#include <vtkPen.h>
#include <vtkGenericOpenGLRenderWindow.h>


namespace Ui
{
  class PlochoUI;
}

class PlochoUI : public QMainWindow
{
	Q_OBJECT

public:
	explicit PlochoUI(QWidget *parent = 0);
	~PlochoUI();
	void show();

	public slots:
	void reset_viewport_clicked();
	void exit_clicked();
	void UpdateWidget();
	void button1_clicked();

protected:

private:
	QSettings settings;
	Ui::PlochoUI *ui;
	vtkSmartPointer<vtkRenderWindow> renderWindow;
	vtkSmartPointer<vtkRenderer> renderer;
	vtkSmartPointer<vtkContextView> view;

};

#endif // MAINWINDOW_H
